const calendarNL = require('./nl/calendar.json');
const errorScreenNL = require('./nl/errorScreen.json');
const eventNL = require('./nl/event.json');
const eventCardNL = require('./nl/eventCard.json');
const eventDetailCardNL = require('./nl/eventDetailCard.json');
const loginNL = require('./nl/login.json');
const pizzaNL = require('./nl/pizza.json');
const profileNL = require('./nl/profile.json');
const registrationNL = require('./nl/registration.json');
const sidebarNL = require('./nl/sidebar.json');
const standardHeaderNL = require('./nl/standardHeader.json');
const welcomeNL = require('./nl/welcome.json');

export default {
  nl: {
    calendar: calendarNL,
    errorScreen: errorScreenNL,
    event: eventNL,
    eventCard: eventCardNL,
    eventDetailCard: eventDetailCardNL,
    login: loginNL,
    pizza: pizzaNL,
    profile: profileNL,
    registration: registrationNL,
    sidebar: sidebarNL,
    standardHeader: standardHeaderNL,
    welcome: welcomeNL,
  },
};
