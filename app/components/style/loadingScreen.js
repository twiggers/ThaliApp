import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  indicatorView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

export default styles;
